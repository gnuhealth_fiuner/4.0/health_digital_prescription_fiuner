# -*- coding: utf-8 -*-
# This file is part of health_digital_prescription_fiuner module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import sys
from dateutil.relativedelta import relativedelta
from datetime import datetime, date

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, Or
from trytond.transaction import Transaction
from trytond.exceptions import UserError, UserWarning


__all__ = ['PatientPrescriptionOrder', 'PrescriptionLine',
    'DeliverPrescriptionLineStart', 'DeliverPrescriptionLine']

MOD_PATIENT_FIUNER = ('trytond.modules.health_patient_fiuner' in sys.modules)


class PatientPrescriptionOrder(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.order'

    external_id = fields.Char('Prescription external ID',
        help="External id, such as REMEDIAR in Argentina",
        states={'readonly': Eval('state') != 'draft'},depends=['state'])
    chronic_patient = fields.Boolean('Chronic patient',
        states={
            'readonly': Or(
                Eval('state') != 'draft',
                Bool(Eval('prescription_line'))),
            },
        depends=['state', 'prescription_line'])
    delivery_duration = fields.Integer('Delivery duration',
        states={
            'readonly': Or(
                Eval('state') != 'draft',
                Bool(Eval('prescription_line'))),
            'required': Bool(Eval('chronic_patient')),
            },
        depends=['state', 'chronic_patient', 'prescription_line'],
        help='Period during which the medication should be delivered '
        'to the patient (in months)')
    delivery_pending = fields.Function(fields.Boolean('Pending deliveries'),
        'get_delivery_pending')
    icon = fields.Function(fields.Char('Icon'), 'get_icon')
    phone = fields.Function(fields.Char('Teléfono'), 'get_party_phone', setter='set_party_phone')
    insurance = fields.Many2One('gnuhealth.insurance','Insurance',
        states={'readonly': Eval('state') != 'draft'},depends=['state'])
    medicament_to_prescribe = fields.Many2One('gnuhealth.medicament','Medicament to prescribe',
        states={'readonly': Eval('state') != 'draft'},depends=['state'])
    medicament_to_prescribe_qty_in_pharmacy = fields.Function(
        fields.Char('Quantity', help='Quantity of medicament on pharmacy'),
        'on_change_with_medicament_to_prescribe_qty_in_pharmacy')

    @classmethod
    def __setup__(cls):
        super(PatientPrescriptionOrder, cls).__setup__()
        if not cls.prescription_line.context:
            cls.prescription_line.context = {}
        cls.prescription_line.context.update({
            'delivery_duration': Eval('delivery_duration'),
            })
        if 'delivery_duration' not in cls.prescription_line.depends:
            cls.prescription_line.depends.append('delivery_duration')
        #cls._error_messages.update({
            #'delete_draft': ('You can not delete prescription "%s" because '
                #'it is not in draft state'),
            #'medicament_qty_unknown': 'The qty of medicament is unknown',
            #})
        cls._buttons.update({
            'prescribe': {
                'invisible': Eval('state')=='done',
                'readonly': ~Eval('medicament_to_prescribe'),
                },
            })

    @staticmethod
    def default_chronic_patient():
        return False

    @fields.depends('patient')
    def on_change_patient(self):
        super(PatientPrescriptionOrder, self).on_change_patient()
        if MOD_PATIENT_FIUNER and self.patient and self.patient.cronico == 'Si':
            self.chronic_patient = True
            self.on_change_chronic_patient()

    @fields.depends('chronic_patient')
    def on_change_chronic_patient(self):
        if self.chronic_patient:
            self.delivery_duration = 3
        else:
            self.delivery_duration = None

    def get_delivery_pending(self, name=None):
        for line in self.prescription_line:
            if line.deliveries < (line.duration or 1):
                return True
        return False

    def get_icon(self, name=None):
        if self.delivery_pending:
            return ''
        return None

    @classmethod
    def delete(cls, prescriptions):
        for prescription in prescriptions:
            if prescription.state != 'draft':
                raise UserError('health_digital_prescription_fiuner.delete_draft', 
                                {'prescription': prescription.rec_name })
        super(PatientPrescriptionOrder, cls).delete(prescriptions)

    @fields.depends('patient')
    def on_change_patient(self, name=None):
        super(PatientPrescriptionOrder, self).on_change_patient()
        if self.patient and self.patient.current_insurance:
            self.insurance = self.patient.current_insurance

    @classmethod
    def get_party_phone(cls, prescriptions, name=None):
        result = {}
        for a in prescriptions:
            result[a.id] = a.patient and a.patient.name.phone or ''
        return result

    @classmethod
    def set_party_phone(cls, prescriptions, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')

        for a in prescriptions:
            if not a.patient:
                continue
            party_phone = ContactMechanism.search([
                ('party', '=', a.patient.name),
                ('type', '=', 'phone'),
                ])
            if party_phone:
                ContactMechanism.write(party_phone, {
                    'value': value,
                    })
            else:
                ContactMechanism.create([{
                    'party': a.patient.name.id,
                    'type': 'phone',
                    'value': value,
                    }])

    @classmethod
    def get_patient_field(cls, prescriptions, names):
        result = {}
        for name in names:
            result[name] = {}
            for a in prescriptions:
                result[name][a.id] = (a.patient and
                    getattr(a.patient, name, None) or None)
        return result

    @classmethod
    @ModelView.button
    def prescribe(cls, prescriptions):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')
        Warning = pool.get('res.user.warning')

        prescription = prescriptions[0]
        warning_name = None

        if float(prescription.medicament_to_prescribe_qty_in_pharmacy) <= 0.0:
            warning_name = '/'.join([str(prescription.medicament_to_prescribe_qty_in_pharmacy), str(prescription.id)])
        if Warning.check(warning_name):
            raise UserWarning(warning_name,
                    'health_digital_prescription_fiuner.medicament_qty_unknown',
                    'There is no stock registered of this medicament')
        PrescriptionLine.create([{
                'name': prescription.id,
                'medicament': prescription.medicament_to_prescribe.id,
                'quantity': prescription.medicament_to_prescribe.name.qty_out,
                'duration': prescription.delivery_duration,
                }])

    @fields.depends('patient')
    def on_change_with_phone(self, name=None):
        return self.get_party_phone([self])[self.id]

    @fields.depends('pharmacy','medicament_to_prescribe')
    def on_change_with_medicament_to_prescribe_qty_in_pharmacy(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        if self.pharmacy and self.medicament_to_prescribe:
            location_id = self.pharmacy.warehouse.id
            with Transaction().set_context(locations=[location_id]):
                quantity = 0
                for product in [x for x in self.medicament_to_prescribe.name.template.products]:
                    product = Product(product.id)
                    quantity += product.quantity
                return str(quantity)
        return '0.0'


class PrescriptionLine(metaclass=PoolMeta):
    __name__ = 'gnuhealth.prescription.line'

    prescription_state = fields.Function(fields.Char('State'),
        'get_prescription_state')
    deliveries = fields.Integer('Deliveries')
    deliveries_string = fields.Function(fields.Char('Deliveries'),
        'get_deliveries_string')
    delivery_available = fields.Function(fields.Boolean('Pending deliveries'),
        'get_delivery_available')
    total_quantity = fields.Function(fields.Integer('Total quantity'),
        'on_change_with_total_quantity')
    stock = fields.Function(fields.Char('Stock on pharmacy'),
        'on_change_with_stock')

    @fields.depends('medicament')
    def on_change_with_quantity(self):
        if self.medicament:
            return self.medicament.name.qty_out

    @classmethod
    def __setup__(cls):
        super(PrescriptionLine, cls).__setup__()
        cls.medicament.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.medicament.depends:
            cls.medicament.depends.append('prescription_state')
        cls.quantity.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.quantity.depends:
            cls.quantity.depends.append('prescription_state')
        cls.indication.states['readonly'] = (
            Eval('prescription_state') == 'done')
        if 'prescription_state' not in cls.indication.depends:
            cls.indication.depends.append('prescription_state')

        cls.duration.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.duration.depends:
            cls.duration.depends.append('prescription_state')
        cls.duration_period.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.duration_period.depends:
            cls.duration_period.depends.append('prescription_state')
        cls.start_treatment.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.start_treatment.depends:
            cls.start_treatment.depends.append('prescription_state')
        cls.end_treatment.states['readonly'] = Or(
            Eval('prescription_state') == 'done',
            Bool(Eval('context', {}).get('delivery_duration', False)))
        if 'prescription_state' not in cls.end_treatment.depends:
            cls.end_treatment.depends.append('prescription_state')
        cls._buttons.update({
            'deliver': {
                'invisible': ~Eval('delivery_available', False),
                },
            })

    @staticmethod
    def default_deliveries():
        return 0

    @staticmethod
    def default_duration():
        return Transaction().context.get('delivery_duration', None)

    @staticmethod
    def default_duration_period():
        if Transaction().context.get('delivery_duration', False):
            return 'months'
        return 'days'

    @staticmethod
    def default_end_treatment():
        duration = Transaction().context.get('delivery_duration', False)
        if duration:
            now = datetime.now()
            return now + relativedelta(months=+duration)

    def get_prescription_state(self, name=None):
        return self.name.state

    def get_deliveries_string(self, name=None):
        return '%s / %s' % (self.deliveries, self.duration or 1)

    def get_delivery_available(self, name=None):
        if (self.name.state == 'done' and
                self.name.pharmacy and
                self.deliveries < (self.duration or 1)):
            return True
        return False

    @fields.depends('quantity', 'duration')
    def on_change_with_total_quantity(self, name=None):
        return self.quantity * (self.duration or 1)

    @fields.depends('quantity', 'name', 'medicament')
    def on_change_with_stock(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        if self.name and self.name.pharmacy and self.medicament:
            location_id = self.name.pharmacy.warehouse.id
            with Transaction().set_context(locations=[location_id]):
                quantity = 0
                for product in [x for x in self.medicament.name.template.products]:
                    product = Product(product.id)
                    quantity +=product.quantity
                return str(quantity)
        return 'Cantidad Indefenida'

    @classmethod
    @ModelView.button_action(
        'health_digital_prescription_fiuner.wizard_deliver_prescription_line')
    def deliver(cls, lines):
        pass


class DeliverPrescriptionLineStart(ModelView):
    'Deliver Prescription Line'
    __name__ = 'gnuhealth.prescription.line.deliver.start'

    custom_gtin = fields.Char('Custom GTIN')
    gtin_indicator_digits = fields.Char('GTIN indicator digits',
        readonly=True)
    gtin = fields.Char('GTIN', readonly=True)
    medicament = fields.Many2One('gnuhealth.medicament', 'Medicament',
        readonly=True)
    serial_number = fields.Char('Serial number',
        help="Introduce serial number, as the qr reading, to get gtin, exp. date, lot, related to a product")
    template = fields.Many2One('product.template', 'Template',
        help='The template of the prescribed medicament')
    product = fields.Many2One('product.product', 'Product',
        help='The product to be delivered')
    lot_found = fields.Boolean('Lot required')
    lot_number = fields.Char('Lot number')
    lot = fields.Many2One('stock.lot', 'Lot',
        domain=[('product', '=', Eval('product'))],
        context={'locations': [Eval('from_location')]},
        depends=['product', 'from_location'])
    expiration_date = fields.Date('Expiration Date', readonly=True)
    quantity = fields.Integer('Quantity')
    prescription = fields.Many2One('gnuhealth.prescription.order',
        'Prescription')
    from_location = fields.Many2One('stock.location', 'From Location')
    to_location = fields.Many2One('stock.location', 'To Location')

    @fields.depends('lot')
    def on_change_with_expiration_date(self):
        if self.lot and self.lot.expiration_date:
            return self.lot.expiration_date
        return None

    @fields.depends('serial_number','lot_found','lot_number','template',
        'lot', 'product','gtin', 'gtin_indicator_digits')
    def on_change_serial_number(self):
        '''
        -ANMAT traceable product code to be parsed:
          Mandatory: 01 + Product GTIN (14) + 21 + Serial Number (10)
          Optional: 10 + Lot (10) + 17 + YYMMDD (expiration date)
          Example: 010773094904669421234219217510P00289A41217190831
        -Common product code (no serial number):
          01 + Product GTIN (14)
          + 17 + YYMMDD (expiration date) [OR] + 10 + Lot (3-6)
          + 90 + Code (7)
          Examples:
            01077962852802601721033110442890RQ8C793
            0107798032934662106041719063090ROAA012
        -Other products codes with length < 36 
          01 + Product GTIN (077962852 + 5 DIGITS) + 1710 
          Example:
            01077962852840081710 

        '''
        Product = Pool().get('product.product')
        Lot = Pool().get('stock.lot')

        code = ''.join([x for x in self.serial_number if x.isalnum()])
        self.serial_number = None
        if code[:len('custom')] == 'custom':
            self.custom_gtin = custom_gtin = code[len('custom'):code.index('expdate')]
            exp_date = code[code.index('expdate')+len('expdate'):code.index('lotnumber')]
            lot_number = code[code.index('lotnumber')+len('lotnumber'):]
            product = Product.search([('custom_gtin','=',custom_gtin)])
            if product:
                self.product = product[0].id
                lot = Lot.search([
                    ('number','=',lot_number),
                    ('product','=',product[0].id)
                    ])
                if lot:
                    self.lot = lot[0].id
                    self.expiration_date = lot[0].expiration_date
                    self.lot_found = True
                    self.lot_number = lot_number
                else:
                    self.lot = None
                    self.lot_found = False
                    self.lot_number = None
            else:
                self.product = None
                self.lot = None
                self.lot_found = False
                self.lot_number = None
        #self.serial_number = None
        #elif code[:len('standard')] == 'standard':
        else:
            #Start looking for the gtin
            if len(code) > 19:
                self.gtin_indicator_digits = gtin_indicator_digits = code[:2]
                self.gtin = gtin = code[2:16]
                #Search the product
                product = Product.search([
                    ('anmat_gtin_indicator_digits','=',gtin_indicator_digits),
                    ('anmat_gtin', '=', gtin),
                    ])
                if product:
                    self.product = product[0].id
                else:
                    self.product = None

                if product and self.product.anmat_traceable:
                    # Traceable
                    lot_number= None
                    self.lot_found = False
                    if code[28:30] == '10' and code[-8:-6] == '17':
                            lot_number = code[30:-8]
                    elif code[28:30] == '17' and code[36:38] == '10':
                            lot_number = code[38:]
                    self.lot_number = lot_number
                    #if there is a lot number and a product, search for the lot
                    if lot_number:
                        lot = Lot.search([
                            ('product','=',product[0].id),
                            ('number','=',lot_number)
                            ])
                        if lot:
                            self.lot_found = True
                            self.lot = lot[0].id
                            if lot[0].expiration_date:
                                self.expiration_date = lot[0].expiration_date
                else:
                    # Not traceable
                    lot_number = None
                    self.lot_found = False
                    if code[16:18] == '17' and code[18:20] != '10' and code[24:26] == '10':
                        lot_number = code[26:-9]
                    elif code[16:18] == '10':
                        lot_number = code[18:-17]
                    self.lot_number = lot_number
                    #If there is a lot number and a product search for the lot
                    if lot_number:
                        lot = Lot.search([('product','=',product[0].id),
                            ('number','=',lot_number)])
                        if lot:
                            self.lot_found = True
                            self.lot = lot[0].id
                            if lot[0].expiration_date:
                                self.expiration_date = lot[0].expiration_date
                    self.serial_number = None


class DeliverPrescriptionLine(Wizard):
    'Deliver Prescription Line'
    __name__ = 'gnuhealth.prescription.line.deliver'

    @classmethod
    def __setup__(cls):
        super(DeliverPrescriptionLine, cls).__setup__()
        #cls._error_messages.update({
            #'product_not_found':
                #'The "%(gtin)s" number is not found',
            #'no_lot_number':
                #'There is no lot number. Do you wish to deliver anyway?',
            #'product_not_match_template':
                #'The product "%(product)s" (to deliver) doesn\'t match the medicament "%(template)s" (prescribed)',
            #'lot_not_registered':
                #'The lot "%(lot)s" is not related to this "%(product)s"',
            #'lot_expired':
                #'The lot "%(lot)s" has expired on "%(expiration_date)s"',
            #})

    start_state = 'check'
    check = StateTransition()
    start = StateView('gnuhealth.prescription.line.deliver.start',
        'health_digital_prescription_fiuner.view_deliver_prescription_line_start_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Confirm', 'confirm', 'tryton-ok', True),
            ])
    confirm = StateTransition()

    def transition_check(self):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')
        line = PrescriptionLine(Transaction().context['active_id'])
        if not line.delivery_available:
            return 'end'
        return 'start'

    def default_start(self, fields):
        pool = Pool()
        PrescriptionLine = pool.get('gnuhealth.prescription.line')

        line = PrescriptionLine(Transaction().context['active_id'])
        prescription = line.name
        from_location = prescription.pharmacy.warehouse
        if from_location.type == 'warehouse':
            from_location = from_location.storage_location
        to_location = prescription.patient.name.customer_location
        product = line.medicament.name

        return {
            'medicament': line.medicament.id,
            'product': product.id,
            'template': product.template.id,
            'quantity': line.quantity,
            'prescription': prescription.id,
            'from_location': from_location.id,
            'to_location': to_location.id,
            }

    def transition_confirm(self):
        pool = Pool()
        StockMove = pool.get('stock.move')
        PrescriptionLine = pool.get('gnuhealth.prescription.line')
        Lot = pool.get('stock.lot')
        Warning = pool.get('res.user.warning')

        #check if the product to deliver exists
        if self.start.product is None:
            raise UserError('health_digital_prescription_fiuner.product_not_found',
                    {'gtin' :'(' + self.start.gtin_indicator_digits + ')' + self.start.gtin })
        #check expiration date
        if self.start.lot and self.start.lot.expiration_date and self.start.expiration_date < date.today():
            raise UserError('health_digital_prescription_fiuner.lot_expired',
                    {
                    'lot': self.start.lot.number,
                    'expiration_date': self.start.lot.expiration_date,
                    })
        #check if there is not a lot number
        if not self.start.lot_number:
            warning_name = '/'.join(['no lot number', str(datetime.today().date()),
                        self.start.product.rec_name])
            if Warning.check(warning_name):
                raise UserWarning(warning_name, 'health_digital_prescription_fiuner.no_lot_number',{})
        #check if the lot is registered on the system
        elif self.start.lot_found == False:
            warning_name = '/'.join(['lot not found',str(datetime.today().date()), self.lot_number])
            if Warning.check(warning_name):
                raise UserWarning(
                    warning_name,
                    'health_digital_prescription_fiuner.lot_not_registered',{
                    'lot': self.start.lot.number,
                    'product': self.start.product.template.name,
                    })
        else:
            lot = Lot.search([
                ('product','=',self.start.product.id),
                ('number','=',self.start.lot.number)
                ])
        #check if the product to deliver is corresponding to the medicament template
        if self.start.product.template.id != self.start.template.id:
            warning_name ='/'.join(['deliver_differente_prescribed', str(datetime.today().date()),
                    self.start.product.rec_name, self.start.template.rec_name])
            if Warning.check(warning_name):
                raise UserWarning(warning_name,
                    'health_digital_prescription_fiuner.product_not_match_template',{
                    'product': self.start.product.template.name,
                    'template': self.start.template.name,
                })
        ##Create Stock Move
        move = StockMove()
        move.origin = self.start.prescription
        move.planned_date = self.start.prescription.prescription_date.date()
        move.effective_date = datetime.today().date()
        move.from_location = self.start.from_location
        move.to_location = self.start.to_location
        move.product = self.start.product
        move.lot = self.start.lot
        move.quantity = self.start.quantity
        move.uom = self.start.product.default_uom
        move.unit_price = self.start.product.list_price
        StockMove.save([move])
        StockMove.do([move])

        # Update deliveries in Prescription line
        line = PrescriptionLine(Transaction().context['active_id'])
        line.deliveries += 1
        line.save()
        return 'end'

