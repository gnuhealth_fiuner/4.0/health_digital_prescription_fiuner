# This file is part of z_health_stock_dangelo module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

try:
    from trytond.modules.z_health_stock_dangelo.tests.tests import suite
except ImportError:
    from .tests import suite

__all__ = ['suite']
