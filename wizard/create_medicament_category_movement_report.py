from trytond.wizard import Wizard, StateView, Button, StateTransition, StateAction
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.pool import Pool 
from trytond.pyson import Eval, Not, Bool, Equal, Or
from trytond.exceptions import UserError


from datetime import datetime, date, time, timedelta

__all__ = ['CreateMedicamentCategoryMovementReportStart',
           'CreateMedicamentCategoryMovementReportWizard']

class CreateMedicamentCategoryMovementReportStart(ModelView):
    'Medicament Category Movement Report Start'
    __name__ = 'gnuhealth.medicament.category.movement_report.start'

    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    category_selection = fields.Selection([
        (None,''),
        ('default','Default'),
        ('choose','Choose categories'),
        ],'Category selection',sort=False,required=True)
    categories = fields.Many2Many('gnuhealth.medicament.category',None,None,
        'Categories', required=True)
    location = fields.Many2One('stock.location','Location',
        required=True, domain=[('type','=','warehouse')])
    medicament_category_none = fields.Boolean(
        'Medicament Category None', help='Also count the medicament without category?')

    @staticmethod
    def default_category_selection():
        return 'choose_categories'


class CreateMedicamentCategoryMovementReportWizard(Wizard):
    'Medicament Category Movement Report Wizard'
    __name__ = 'gnuhealth.medicament.category.movement_report.wizard'

    @classmethod
    def __setup__(cls):
        super(CreateMedicamentCategoryMovementReportWizard,cls).__setup__()
        #cls._error_messages.update({
            #'end_date_before_start_date': 'The end date cannot be major thant the start date',
            #})

    start = StateView('gnuhealth.medicament.category.movement_report.start',
                      'health_digital_prescription_fiuner.create_medicament_cat_move_report_start_form',[
                        Button('Cancel','end','tryton-cancel'),
                        Button('Print Report','prevalidate','tryton-ok',default=True),
                       ])

    prevalidate = StateTransition()

    create_report =\
        StateAction('health_digital_prescription_fiuner.act_gnuhealth_medicament_category_movement_report')

    def default_start(self,fields):
        return{
            'end_date':datetime.now()
            }

    def transition_prevalidate(self):
        if self.start.end_date < self.start.start_date:
            raise UserError('health_digital_prescription_fiuner.end_date_before_start_date')
        return 'create_report'

    def fill_data(self):
        start = self.start.start_date
        end = self.start.end_date
        return {
            'start':start,
            'end':end
            }

    def do_create_report(self, action):
        data = self.fill_data()
        data['location'] = self.start.location.id
        data['category_selection'] = self.start.category_selection
        data['categories'] = [x.id for x in self.start.categories]
        data['medicament_category_none'] = self.start.medicament_category_none
        return action, data
