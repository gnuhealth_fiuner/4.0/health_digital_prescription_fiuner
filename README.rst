.. image:: https://www.gnuhealth.org/downloads/artwork/logos/isologo-gnu-health.png
.. image:: https://lh3.googleusercontent.com/proxy/kM5eWnJBE5mK_0dFLc-EJjBbI4kq9QxFhvNdx9P7z6VqwRxSxqzpYqElCIN8E4eryEqEOBGKsrqy2FWxoXcFi2nHOo9UnX8TL356m4hKn8if45s7MD9v6s7VF-jN

GNU Health HMIS: Libre Hospital Management and Health Information System
========================================================================

Health Digital Prescription FIUNER
==================================
Este módulo tiene un asistente que permite, desde las líneas de
prescripciones, crear movimientos de stock mensuales de los medicamentos
según se haya indicado (para pacientes crónicos, por ejemplo).

Es un derivado del modulo hecho a medida z_health_stock_dangelo.


This module provides, among other features, a wizard accesible trough a button 
that allows to create stock movements from each medicament line according to the
prescription (e.g. for chronical patients).

It is a derivative of the custom-made module z_health_stock_dangelo. 


Documentation
-------------
*GNU Health
Wikibooks: https://en.wikibooks.org/wiki/GNU_Health/

Contact
-------
* GNU Health Contact 
 - website: https://www.gnuhealth.org
 - email: info@gnuhealth.org
 - Twitter: @gnuhealth

* FIUNER Contact 
 - email: saludpublica@ingenieria.uner.edu.ar

* SILIX contact 
 - website: http://www.silix.com.ar 
 - email: contacto@silix.com.ar 

License
--------

GNU Health is licensed under GPL v3+::

 Copyright (C) 2008-2021 Luis Falcon <falcon@gnuhealth.org>
 Copyright (C) 2011-2021 GNU Solidario <health@gnusolidario.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


Prerequisites
-------------

 * Python 3.4 or later (http://www.python.org/)
 * Tryton 5.0 (http://www.tryton.org/)


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
